import { Button, Drawer, Icon, List } from 'antd';
import { get } from 'lodash';
import { observer } from 'mobx-react';
import React, { FC } from 'react';
import styled from 'styled-components';
import { useStores } from '~stores';
import { CURRENCY_FORMAT, formatNumber } from '~utils/formatNumber';

const { Item } = List;
const { Meta } = Item;

const StyledImage = styled.img`
  width: 100px;
`;

const StyledContent = styled(Meta)`
  padding: 10px;
  width: 100%;
`;

const StyledPrice = styled.div`
  color: #5b5a5e;
  font-size: 18px;
`;

const StyledFooter = styled.div`
  box-sizing: border-box;
  padding: 5%;
  position: absolute;
  bottom: 0;
  left: 0;
  width: 100%;
  z-index: 2;
  background-color: #001529;
`;

const StyledTotal = styled.div`
  display: flex;
  align-items: center;
  justify-content: space-between;
`;

const StyledTitle = styled.div`
  font-size: 18px;
  color: #fff;
  text-transform: uppercase;
`;

const StyledButton = styled(Button)`
  color: #ececec;
  outline-color: #5b5a5e;
  margin-top: 20px;
`;

const ShoppingCart: FC = observer(() => {
  const {
    layoutStore: { isShoppingCartCollapse, toggleShoppingCart },
    shoppingCartStore: { cart, addItem, removeItem, total }
  } = useStores();

  return (
    <Drawer
      title="Giỏ hàng"
      placement="right"
      width={window.innerWidth > 640 ? 520 : "100%"}
      onClose={() => toggleShoppingCart()}
      visible={isShoppingCartCollapse}
    >
      <List
        itemLayout="horizontal"
        dataSource={cart}
        renderItem={item => (
          <Item
            actions={[
              <Icon type="plus" key="plus" onClick={() => addItem(item)} />,
              <Icon
                type="minus"
                key="minus"
                onClick={() => addItem(item, -1)}
              />,
              <Icon type="close" key="close" onClick={() => removeItem(item)} />
            ]}
          >
            <StyledImage
              alt={get(item, "display")}
              src={get(
                item,
                "images[0].path",
                require("../../assets/images/404.png")
              )}
            />
            <StyledContent
              title={
                <a href="https://ant.design">{get(item, "display", "")}</a>
              }
              description={<div>SL: {get(item, "amount", 0)}</div>}
            />
            <StyledPrice>
              {formatNumber(get(item, "price", 0), CURRENCY_FORMAT)}
            </StyledPrice>
          </Item>
        )}
      />
      <StyledFooter>
        <StyledTotal>
          <StyledTitle>Tổng tiền:</StyledTitle>
          <StyledPrice style={{ color: "#eabf00" }}>
            {formatNumber(get(total, "price", 0), CURRENCY_FORMAT)}
          </StyledPrice>
        </StyledTotal>
        <StyledButton
          type="danger"
          disabled={total.price <= 0}
          block={true}
          size="large"
        >
          Thanh toán
        </StyledButton>
      </StyledFooter>
    </Drawer>
  );
});

export default ShoppingCart;
