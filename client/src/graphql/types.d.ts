

/* tslint:disable */
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL mutation operation: ChangePassword
// ====================================================

export interface ChangePassword_changePassword_data_profile {
  display: string;
  roleIds: string[];
}

export interface ChangePassword_changePassword_data_creator {
  fullname: string;
  username: string;
}

export interface ChangePassword_changePassword_data {
  _id: string;
  fullname: string;
  username: string;
  dob: any | null;
  phone: string | null;
  address: string | null;
  status: boolean | null;
  profileId: string;
  profile: ChangePassword_changePassword_data_profile;
  creatorId: string | null;
  creator: ChangePassword_changePassword_data_creator | null;
  createdAt: any | null;
  updatedAt: any | null;
}

export interface ChangePassword_changePassword_error {
  title: string | null;
  message: string;
}

export interface ChangePassword_changePassword {
  data: ChangePassword_changePassword_data | null;
  error: ChangePassword_changePassword_error | null;
}

export interface ChangePassword {
  changePassword: ChangePassword_changePassword;
}

export interface ChangePasswordVariables {
  newPassword: string;
  oldPassword: string;
}


/* tslint:disable */
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL mutation operation: CreateCommon
// ====================================================

export interface CreateCommon_createCommon_data {
  _id: string;
  key: string | null;
  value: string;
  type: string;
  meta: any | null;
  createdAt: any | null;
  updatedAt: any | null;
}

export interface CreateCommon_createCommon_error {
  title: string | null;
  message: string;
}

export interface CreateCommon_createCommon {
  data: CreateCommon_createCommon_data | null;
  error: CreateCommon_createCommon_error | null;
}

export interface CreateCommon {
  createCommon: CreateCommon_createCommon;
}

export interface CreateCommonVariables {
  record: CommonInput;
}


/* tslint:disable */
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL mutation operation: CreateFile
// ====================================================

export interface CreateFile_createFile_data {
  id: string;
  path: string | null;
}

export interface CreateFile_createFile_error {
  title: string | null;
  message: string;
}

export interface CreateFile_createFile {
  data: CreateFile_createFile_data | null;
  error: CreateFile_createFile_error | null;
}

export interface CreateFile {
  createFile: CreateFile_createFile;
}


/* tslint:disable */
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL mutation operation: CreateMessage
// ====================================================

export interface CreateMessage_createMessage_data_receiever {
  fullname: string;
  username: string;
}

export interface CreateMessage_createMessage_data_creator {
  fullname: string;
  username: string;
}

export interface CreateMessage_createMessage_data {
  _id: string;
  content: string;
  receieverId: string | null;
  receiever: CreateMessage_createMessage_data_receiever | null;
  creatorId: string | null;
  creator: CreateMessage_createMessage_data_creator | null;
  createdAt: any | null;
  updatedAt: any | null;
}

export interface CreateMessage_createMessage_error {
  title: string | null;
  message: string;
}

export interface CreateMessage_createMessage {
  data: CreateMessage_createMessage_data | null;
  error: CreateMessage_createMessage_error | null;
}

export interface CreateMessage {
  createMessage: CreateMessage_createMessage;
}

export interface CreateMessageVariables {
  record: MessageInput;
}


/* tslint:disable */
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL mutation operation: CreateProduct
// ====================================================

export interface CreateProduct_createProduct_data_brand {
  value: string;
}

export interface CreateProduct_createProduct_data_category {
  value: string;
}

export interface CreateProduct_createProduct_data_images {
  id: string;
  path: string | null;
}

export interface CreateProduct_createProduct_data_creator {
  fullname: string;
  username: string;
}

export interface CreateProduct_createProduct_data {
  _id: string;
  sku: string;
  display: string;
  brandId: string;
  brand: CreateProduct_createProduct_data_brand | null;
  categoryId: string;
  category: CreateProduct_createProduct_data_category | null;
  cost: number;
  price: number;
  discount: number;
  quantity: number;
  images: CreateProduct_createProduct_data_images[];
  creatorId: string | null;
  creator: CreateProduct_createProduct_data_creator | null;
  createdAt: any | null;
  updatedAt: any | null;
}

export interface CreateProduct_createProduct_error {
  title: string | null;
  message: string;
}

export interface CreateProduct_createProduct {
  data: CreateProduct_createProduct_data | null;
  error: CreateProduct_createProduct_error | null;
}

export interface CreateProduct {
  createProduct: CreateProduct_createProduct;
}

export interface CreateProductVariables {
  record: ProductInput;
}


/* tslint:disable */
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL mutation operation: Login
// ====================================================

export interface Login_login_error {
  title: string | null;
  message: string;
}

export interface Login_login {
  authToken: string | null;
  error: Login_login_error | null;
}

export interface Login {
  login: Login_login;
}

export interface LoginVariables {
  username: string;
  password: string;
}


/* tslint:disable */
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL mutation operation: RemoveCommonById
// ====================================================

export interface RemoveCommonById_removeCommonById_data {
  _id: string;
  key: string | null;
  value: string;
  type: string;
  meta: any | null;
  createdAt: any | null;
  updatedAt: any | null;
}

export interface RemoveCommonById_removeCommonById_error {
  title: string | null;
  message: string;
}

export interface RemoveCommonById_removeCommonById {
  data: RemoveCommonById_removeCommonById_data | null;
  error: RemoveCommonById_removeCommonById_error | null;
}

export interface RemoveCommonById {
  removeCommonById: RemoveCommonById_removeCommonById;
}

export interface RemoveCommonByIdVariables {
  _id: string;
}


/* tslint:disable */
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL mutation operation: RemoveFileById
// ====================================================

export interface RemoveFileById_removeFileById_data {
  id: string;
}

export interface RemoveFileById_removeFileById_error {
  title: string | null;
  message: string;
}

export interface RemoveFileById_removeFileById {
  data: RemoveFileById_removeFileById_data | null;
  error: RemoveFileById_removeFileById_error | null;
}

export interface RemoveFileById {
  removeFileById: RemoveFileById_removeFileById;
}

export interface RemoveFileByIdVariables {
  id: string;
}


/* tslint:disable */
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL mutation operation: RemoveProductById
// ====================================================

export interface RemoveProductById_removeProductById_data_brand {
  value: string;
}

export interface RemoveProductById_removeProductById_data_category {
  value: string;
}

export interface RemoveProductById_removeProductById_data_images {
  id: string;
  path: string | null;
}

export interface RemoveProductById_removeProductById_data_creator {
  fullname: string;
  username: string;
}

export interface RemoveProductById_removeProductById_data {
  _id: string;
  sku: string;
  display: string;
  brandId: string;
  brand: RemoveProductById_removeProductById_data_brand | null;
  categoryId: string;
  category: RemoveProductById_removeProductById_data_category | null;
  cost: number;
  price: number;
  discount: number;
  quantity: number;
  images: RemoveProductById_removeProductById_data_images[];
  creatorId: string | null;
  creator: RemoveProductById_removeProductById_data_creator | null;
  createdAt: any | null;
  updatedAt: any | null;
}

export interface RemoveProductById_removeProductById_error {
  title: string | null;
  message: string;
}

export interface RemoveProductById_removeProductById {
  data: RemoveProductById_removeProductById_data | null;
  error: RemoveProductById_removeProductById_error | null;
}

export interface RemoveProductById {
  removeProductById: RemoveProductById_removeProductById;
}

export interface RemoveProductByIdVariables {
  _id: string;
}


/* tslint:disable */
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL mutation operation: UpdateCommonById
// ====================================================

export interface UpdateCommonById_updateCommonById_data {
  _id: string;
  key: string | null;
  value: string;
  type: string;
  meta: any | null;
  createdAt: any | null;
  updatedAt: any | null;
}

export interface UpdateCommonById_updateCommonById_error {
  title: string | null;
  message: string;
}

export interface UpdateCommonById_updateCommonById {
  data: UpdateCommonById_updateCommonById_data | null;
  error: UpdateCommonById_updateCommonById_error | null;
}

export interface UpdateCommonById {
  updateCommonById: UpdateCommonById_updateCommonById;
}

export interface UpdateCommonByIdVariables {
  _id: string;
  record: CommonUpdateArg;
}


/* tslint:disable */
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL mutation operation: UpdateInfo
// ====================================================

export interface UpdateInfo_updateInfo_data_profile {
  display: string;
  roleIds: string[];
}

export interface UpdateInfo_updateInfo_data_creator {
  fullname: string;
  username: string;
}

export interface UpdateInfo_updateInfo_data {
  _id: string;
  fullname: string;
  username: string;
  dob: any | null;
  phone: string | null;
  address: string | null;
  status: boolean | null;
  profileId: string;
  profile: UpdateInfo_updateInfo_data_profile;
  creatorId: string | null;
  creator: UpdateInfo_updateInfo_data_creator | null;
  createdAt: any | null;
  updatedAt: any | null;
}

export interface UpdateInfo_updateInfo_error {
  title: string | null;
  message: string;
}

export interface UpdateInfo_updateInfo {
  data: UpdateInfo_updateInfo_data | null;
  error: UpdateInfo_updateInfo_error | null;
}

export interface UpdateInfo {
  updateInfo: UpdateInfo_updateInfo;
}

export interface UpdateInfoVariables {
  record: UpdateInfoInput;
}


/* tslint:disable */
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL mutation operation: UpdateProductById
// ====================================================

export interface UpdateProductById_updateProductById_data_brand {
  value: string;
}

export interface UpdateProductById_updateProductById_data_category {
  value: string;
}

export interface UpdateProductById_updateProductById_data_images {
  id: string;
  path: string | null;
}

export interface UpdateProductById_updateProductById_data_creator {
  fullname: string;
  username: string;
}

export interface UpdateProductById_updateProductById_data {
  _id: string;
  sku: string;
  display: string;
  brandId: string;
  brand: UpdateProductById_updateProductById_data_brand | null;
  categoryId: string;
  category: UpdateProductById_updateProductById_data_category | null;
  cost: number;
  price: number;
  discount: number;
  quantity: number;
  images: UpdateProductById_updateProductById_data_images[];
  creatorId: string | null;
  creator: UpdateProductById_updateProductById_data_creator | null;
  createdAt: any | null;
  updatedAt: any | null;
}

export interface UpdateProductById_updateProductById_error {
  title: string | null;
  message: string;
}

export interface UpdateProductById_updateProductById {
  data: UpdateProductById_updateProductById_data | null;
  error: UpdateProductById_updateProductById_error | null;
}

export interface UpdateProductById {
  updateProductById: UpdateProductById_updateProductById;
}

export interface UpdateProductByIdVariables {
  _id: string;
  record: ProductUpdateArg;
}


/* tslint:disable */
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL query operation: CountCommon
// ====================================================

export interface CountCommon {
  countCommon: number | null;
}

export interface CountCommonVariables {
  where?: CommonFilter | null;
}


/* tslint:disable */
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL query operation: CountMessage
// ====================================================

export interface CountMessage {
  countMessage: number | null;
}

export interface CountMessageVariables {
  where?: MessageFilter | null;
}


/* tslint:disable */
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL query operation: CountProduct
// ====================================================

export interface CountProduct {
  countProduct: number | null;
}

export interface CountProductVariables {
  where?: ProductFilter | null;
}


/* tslint:disable */
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL query operation: FindFileById
// ====================================================

export interface FindFileById_findFileById_data {
  id: string;
  path: string | null;
}

export interface FindFileById_findFileById_error {
  title: string | null;
  message: string;
}

export interface FindFileById_findFileById {
  data: FindFileById_findFileById_data | null;
  error: FindFileById_findFileById_error | null;
}

export interface FindFileById {
  findFileById: FindFileById_findFileById;
}

export interface FindFileByIdVariables {
  id: string;
}


/* tslint:disable */
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL query operation: FindManyCommon
// ====================================================

export interface FindManyCommon_findManyCommon {
  _id: string;
  key: string | null;
  value: string;
  type: string;
  meta: any | null;
  createdAt: any | null;
  updatedAt: any | null;
}

export interface FindManyCommon {
  findManyCommon: FindManyCommon_findManyCommon[];
}

export interface FindManyCommonVariables {
  where?: CommonFilter | null;
  limit?: number | null;
  skip?: number | null;
  sort?: CommonSort | null;
}


/* tslint:disable */
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL query operation: FindManyMessage
// ====================================================

export interface FindManyMessage_findManyMessage_receiever {
  fullname: string;
  username: string;
}

export interface FindManyMessage_findManyMessage_creator {
  fullname: string;
  username: string;
}

export interface FindManyMessage_findManyMessage {
  _id: string;
  content: string;
  receieverId: string | null;
  receiever: FindManyMessage_findManyMessage_receiever | null;
  creatorId: string | null;
  creator: FindManyMessage_findManyMessage_creator | null;
  createdAt: any | null;
  updatedAt: any | null;
}

export interface FindManyMessage {
  findManyMessage: FindManyMessage_findManyMessage[];
}

export interface FindManyMessageVariables {
  where?: MessageFilter | null;
  limit?: number | null;
  skip?: number | null;
  sort?: MessageSort | null;
}


/* tslint:disable */
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL query operation: FindManyProduct
// ====================================================

export interface FindManyProduct_findManyProduct_brand {
  value: string;
}

export interface FindManyProduct_findManyProduct_category {
  value: string;
}

export interface FindManyProduct_findManyProduct_images {
  id: string;
  path: string | null;
}

export interface FindManyProduct_findManyProduct_creator {
  fullname: string;
  username: string;
}

export interface FindManyProduct_findManyProduct {
  _id: string;
  sku: string;
  display: string;
  brandId: string;
  brand: FindManyProduct_findManyProduct_brand | null;
  categoryId: string;
  category: FindManyProduct_findManyProduct_category | null;
  cost: number;
  price: number;
  discount: number;
  quantity: number;
  images: FindManyProduct_findManyProduct_images[];
  creatorId: string | null;
  creator: FindManyProduct_findManyProduct_creator | null;
  createdAt: any | null;
  updatedAt: any | null;
}

export interface FindManyProduct {
  findManyProduct: FindManyProduct_findManyProduct[];
}

export interface FindManyProductVariables {
  where?: ProductFilter | null;
  limit?: number | null;
  skip?: number | null;
  sort?: ProductSort | null;
}


/* tslint:disable */
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL query operation: FindOneProduct
// ====================================================

export interface FindOneProduct_findOneProduct_brand {
  value: string;
}

export interface FindOneProduct_findOneProduct_category {
  value: string;
}

export interface FindOneProduct_findOneProduct_images {
  id: string;
  path: string | null;
}

export interface FindOneProduct_findOneProduct_creator {
  fullname: string;
  username: string;
}

export interface FindOneProduct_findOneProduct {
  _id: string;
  sku: string;
  display: string;
  brandId: string;
  brand: FindOneProduct_findOneProduct_brand | null;
  categoryId: string;
  category: FindOneProduct_findOneProduct_category | null;
  cost: number;
  price: number;
  discount: number;
  quantity: number;
  images: FindOneProduct_findOneProduct_images[];
  creatorId: string | null;
  creator: FindOneProduct_findOneProduct_creator | null;
  createdAt: any | null;
  updatedAt: any | null;
}

export interface FindOneProduct {
  findOneProduct: FindOneProduct_findOneProduct | null;
}

export interface FindOneProductVariables {
  where?: ProductFilter | null;
  sort?: ProductSort | null;
}


/* tslint:disable */
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL query operation: Me
// ====================================================

export interface Me_me_data_profile {
  display: string;
  roleIds: string[];
}

export interface Me_me_data_creator {
  fullname: string;
  username: string;
}

export interface Me_me_data {
  _id: string;
  fullname: string;
  username: string;
  dob: any | null;
  phone: string | null;
  address: string | null;
  status: boolean | null;
  profileId: string;
  profile: Me_me_data_profile;
  creatorId: string | null;
  creator: Me_me_data_creator | null;
  createdAt: any | null;
  updatedAt: any | null;
}

export interface Me_me {
  data: Me_me_data | null;
}

export interface Me {
  me: Me_me;
}


/* tslint:disable */
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL subscription operation: NewMessage
// ====================================================

export interface NewMessage_newMessage_receiever {
  fullname: string;
  username: string;
}

export interface NewMessage_newMessage_creator {
  fullname: string;
  username: string;
}

export interface NewMessage_newMessage {
  _id: string;
  content: string;
  receieverId: string | null;
  receiever: NewMessage_newMessage_receiever | null;
  creatorId: string | null;
  creator: NewMessage_newMessage_creator | null;
  createdAt: any | null;
  updatedAt: any | null;
}

export interface NewMessage {
  newMessage: NewMessage_newMessage;
}


/* tslint:disable */
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL fragment: Common
// ====================================================

export interface Common {
  _id: string;
  key: string | null;
  value: string;
  type: string;
  meta: any | null;
  createdAt: any | null;
  updatedAt: any | null;
}


/* tslint:disable */
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL fragment: Message
// ====================================================

export interface Message_receiever {
  fullname: string;
  username: string;
}

export interface Message_creator {
  fullname: string;
  username: string;
}

export interface Message {
  _id: string;
  content: string;
  receieverId: string | null;
  receiever: Message_receiever | null;
  creatorId: string | null;
  creator: Message_creator | null;
  createdAt: any | null;
  updatedAt: any | null;
}


/* tslint:disable */
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL fragment: Product
// ====================================================

export interface Product_brand {
  value: string;
}

export interface Product_category {
  value: string;
}

export interface Product_images {
  id: string;
  path: string | null;
}

export interface Product_creator {
  fullname: string;
  username: string;
}

export interface Product {
  _id: string;
  sku: string;
  display: string;
  brandId: string;
  brand: Product_brand | null;
  categoryId: string;
  category: Product_category | null;
  cost: number;
  price: number;
  discount: number;
  quantity: number;
  images: Product_images[];
  creatorId: string | null;
  creator: Product_creator | null;
  createdAt: any | null;
  updatedAt: any | null;
}


/* tslint:disable */
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL fragment: User
// ====================================================

export interface User_profile {
  display: string;
  roleIds: string[];
}

export interface User_creator {
  fullname: string;
  username: string;
}

export interface User {
  _id: string;
  fullname: string;
  username: string;
  dob: any | null;
  phone: string | null;
  address: string | null;
  status: boolean | null;
  profileId: string;
  profile: User_profile;
  creatorId: string | null;
  creator: User_creator | null;
  createdAt: any | null;
  updatedAt: any | null;
}

/* tslint:disable */
// This file was automatically generated and should not be edited.

//==============================================================
// START Enums and Input Objects
//==============================================================

export enum SortDirection {
  ASC = "ASC",
  DESC = "DESC",
}

// null
export interface CommonInput {
  key?: string | null;
  value: string;
  type: string;
  meta?: any | null;
}

// null
export interface MessageInput {
  content: string;
  receieverId?: string | null;
  creatorId?: string | null;
}

// null
export interface ProductInput {
  sku: string;
  display: string;
  categoryId: string;
  brandId: string;
  imageIds?: string[] | null;
  cost: number;
  price: number;
  discount: number;
  quantity: number;
  creatorId?: string | null;
}

// null
export interface CommonUpdateArg {
  key?: string | null;
  value?: string | null;
  type?: string | null;
  meta?: any | null;
}

// null
export interface UpdateInfoInput {
  fullname: string;
  address?: string | null;
  phone?: string | null;
  dob?: any | null;
}

// null
export interface ProductUpdateArg {
  sku?: string | null;
  display?: string | null;
  categoryId?: string | null;
  brandId?: string | null;
  imageIds?: string[] | null;
  cost?: number | null;
  price?: number | null;
  discount?: number | null;
  quantity?: number | null;
}

// null
export interface CommonFilter {
  _ids?: string[] | null;
  AND?: CommonFilter[] | null;
  OR?: CommonFilter[] | null;
  key?: string | null;
  value?: string | null;
  type?: string | null;
  createdAt?: any | null;
  updatedAt?: any | null;
  _id?: string | null;
  _operators?: CommonFilterOperator | null;
  _search?: string | null;
}

// null
export interface CommonFilterOperator {
  key?: StringOperatorArgs | null;
  value?: StringOperatorArgs | null;
  type?: StringOperatorArgs | null;
  createdAt?: DateOperatorArgs | null;
  updatedAt?: DateOperatorArgs | null;
}

// null
export interface StringOperatorArgs {
  in?: string[] | null;
  nin?: string[] | null;
}

// null
export interface DateOperatorArgs {
  gt?: any | null;
  gte?: any | null;
  lt?: any | null;
  lte?: any | null;
}

// null
export interface MessageFilter {
  _ids?: string[] | null;
  AND?: MessageFilter[] | null;
  OR?: MessageFilter[] | null;
  content?: string | null;
  receieverId?: string | null;
  creatorId?: string | null;
  createdAt?: any | null;
  updatedAt?: any | null;
  _id?: string | null;
  _operators?: MessageFilterOperator | null;
  _search?: string | null;
}

// null
export interface MessageFilterOperator {
  content?: StringOperatorArgs | null;
  createdAt?: DateOperatorArgs | null;
  updatedAt?: DateOperatorArgs | null;
}

// null
export interface ProductFilter {
  _ids?: string[] | null;
  AND?: ProductFilter[] | null;
  OR?: ProductFilter[] | null;
  sku?: string | null;
  display?: string | null;
  categoryId?: string | null;
  brandId?: string | null;
  cost?: number | null;
  price?: number | null;
  discount?: number | null;
  quantity?: number | null;
  creatorId?: string | null;
  createdAt?: any | null;
  updatedAt?: any | null;
  _id?: string | null;
  _operators?: ProductFilterOperator | null;
  _search?: string | null;
}

// null
export interface ProductFilterOperator {
  sku?: StringOperatorArgs | null;
  display?: StringOperatorArgs | null;
  categoryId?: StringOperatorArgs | null;
  brandId?: StringOperatorArgs | null;
  cost?: NumberOperatorArgs | null;
  price?: NumberOperatorArgs | null;
  discount?: NumberOperatorArgs | null;
  quantity?: NumberOperatorArgs | null;
  createdAt?: DateOperatorArgs | null;
  updatedAt?: DateOperatorArgs | null;
}

// null
export interface NumberOperatorArgs {
  in?: number[] | null;
  nin?: number[] | null;
  gt?: number | null;
  gte?: number | null;
  lt?: number | null;
  lte?: number | null;
}

// null
export interface CommonSort {
  key?: SortDirection | null;
  value?: SortDirection | null;
  type?: SortDirection | null;
  createdAt?: SortDirection | null;
  updatedAt?: SortDirection | null;
}

// null
export interface MessageSort {
  content?: SortDirection | null;
  receieverId?: SortDirection | null;
  creatorId?: SortDirection | null;
  createdAt?: SortDirection | null;
  updatedAt?: SortDirection | null;
}

// null
export interface ProductSort {
  sku?: SortDirection | null;
  display?: SortDirection | null;
  categoryId?: SortDirection | null;
  brandId?: SortDirection | null;
  cost?: SortDirection | null;
  price?: SortDirection | null;
  discount?: SortDirection | null;
  quantity?: SortDirection | null;
  creatorId?: SortDirection | null;
  createdAt?: SortDirection | null;
  updatedAt?: SortDirection | null;
}

//==============================================================
// END Enums and Input Objects
//==============================================================